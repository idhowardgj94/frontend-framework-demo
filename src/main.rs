use actix_web::{get, post, web, App, HttpServer, middleware, HttpResponse};
use serde::{Deserialize, Serialize};
use serde_json::{Value};
use std::fs;

#[cfg(test)]
mod test {
    use std::fs;
    
    #[test]
    fn read_json() {
        let contents = fs::read_to_string("src/demo.json");
        if let Ok(str) = contents {
            assert_eq!("ok", str);
        } else {
            assert_eq!("fail", "no");
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Response {
    status: String,
    msg: String
}

impl Response {
    fn success() -> Self {
        Response {
            status: "success".to_owned(),
            msg: "you are login!".to_owned()
        }
    }

    fn error() -> Self {
        Response {
            status: "fail".to_owned(),
            msg: "check account or password".to_owned()
        }
    }
}

#[get("/api/v1/data")]
async fn get() -> HttpResponse {
    let contents = fs::read_to_string("src/demo.json");
    let data: Value = if let Ok(str) = contents {
         serde_json::from_str(&str).unwrap()
    } else {
         serde_json::from_str(r#"
                {
                    "status": "success"
                }
        "#).unwrap()
    };

    HttpResponse::Ok().json(data)
}

#[post("/api/v1/login")]
async fn index(item: web::Json<Value>) -> HttpResponse {
    if item["username"] == "howard" && item["password"] == "howard" {
        return HttpResponse::Ok().json(Response::success())
    } 

    HttpResponse::Ok().json(Response::error())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Logger::default())
            .data(web::JsonConfig::default().limit(4096)) 
            .service(index)
            .service(get)
    })
        .bind("0.0.0.0:3001")?
        .run()
        .await
}

