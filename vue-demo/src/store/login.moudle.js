import router  from "../router";

export default {
  namespaced: true,
  state: {
    value: { status: "init", data: [] }
  },
  getters: {
    lState: (state) => {
      return state.value.status;
    }
  },
  mutations: {
    loginRequest(state, payload) {
      state.value = payload;
    },
  },
  actions: {
    login({ commit }, payload) {
      fetch("/api/v1/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      })
        .then((r) => r.json())
        .then((d) => {
          if (d.status === "success") {
            localStorage.setItem("login-check", true);
            router.push("/home");
          } else {
            commit("loginRequest", { status: "fetch" });
          }
        });
    },
  },
};
