import Vue from "vue";
import Vuex from "vuex";
import login from "./login.moudle";
import data from "./data.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login,
    data
  }
});
