export default {
  namespaced: true,
  state: {
    status: "init",
    data: []
  },
  getters: {
    data: state => state.data,
    status: state => state.status
  },
  mutations: {
    fetchDataRequest(state, { status, data }) {
      state.status = status;
      state.data = data;
    }
  },
  actions: {
    fetchData({ commit }) {
      fetch("/api/v1/data", {
        method: "GET"
      })
        .then(r => r.json())
        .then(d => {
          console.log('inside success', d)
          commit("fetchDataRequest", d);
        });
    },
    updateValue({ commit }, payload) {
      commit("fetchDataRequest", payload);
    }
  }
};
