import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Transient from "../views/Transient.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "transient",
    component: Transient
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    beforeEnter: (to, from, next) => {
      const loginChcek = localStorage.getItem("login-check");
      if (loginChcek !== undefined && loginChcek !== null) {
        next({ name: "home" });
      } else {
        next();
      }
    }
  },
  {
    path: "/home",
    name: "home",
    component: Home,
    beforeEnter: (to, from, next) => {
      const loginChcek = localStorage.getItem("login-check");
      if (loginChcek === undefined || loginChcek === null) {
        console.log("what happen again");
        next({ name: "login" });
      } else {
        next();
      }
    }
  },
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
