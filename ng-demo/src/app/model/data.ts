export interface Data {
  id: number
  name: string
  height: number
  weight: number
}