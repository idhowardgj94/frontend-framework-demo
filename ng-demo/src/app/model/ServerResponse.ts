export interface ServerResponse<T> {
  status: "success" | "fail"
  data: T
  msg?: string
}