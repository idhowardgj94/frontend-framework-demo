import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
import { LoginPayload } from '../model/login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMsg: string = '';

  loginForm = this.fb.group({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  
  constructor(private fb: FormBuilder, private service: LoginService) {

  }

  get form() {
    return this.loginForm.controls
  }
  
  ngOnInit(): void {
    // this is a life cycle
    this.service.isLogin();
  }


  onSubmit() {
    console.log("inside onSubmit");
    if(!this.form.username.errors && !this.form.password.errors) {
      const payload: LoginPayload = {
        ...this.loginForm.value
      } 
  
      this.service.login(payload).subscribe(o => {
        if(o.status === 'fail') {
          this.errorMsg = "帳號密碼錯誤，請檢查"
        }
      })
    }
  }
}
