import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DataService } from '../data.service';
import { Data } from '../model/data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loading: boolean = false;
  data$: Observable<any>;
  constructor(private service: DataService) { }

  ngOnInit(): void {
    this.data$ = this.service.getData()
      .pipe(
        map(r => r.data),
        map(d => d.map(it => ({...it, height: `${it.height} 公分`, weight: `${it.weight} 公斤`})))
      );
    this.data$.subscribe(d => { console.log(d); this.loading = true })
  }

}
