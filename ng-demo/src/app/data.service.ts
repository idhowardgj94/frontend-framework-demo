import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data } from './model/data';
import { ServerResponse } from './model/ServerResponse';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get<ServerResponse<Data[]>>('api/v1/data')
  }
}
