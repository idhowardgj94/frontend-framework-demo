import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      // 判斷是否登入
      if(localStorage.getItem("login-check") === null || localStorage.getItem("login-check") === undefined) {
        this.router.navigate(["/login"]);
      }

      return localStorage.getItem("login-check") !== null && localStorage.getItem("login-check") !== undefined;
  }
  
}
