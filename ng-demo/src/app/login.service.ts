import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators'
import { LoginPayload } from './model/login';
import { ServerResponse } from './model/ServerResponse';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  isLogin() {
    if(localStorage.getItem("login-check") !== null && localStorage.getItem("login-check") !== undefined) {
      console.log("inside isLogin", localStorage.getItem("login-check"))
      this.router.navigate(["/home"]);
    }
  }

  login(login: LoginPayload): Observable<ServerResponse<never>> {
    return this.http.post<ServerResponse<never>>("/api/v1/login", login)
      .pipe(
        tap(res => res.status === "success" ? localStorage.setItem("login-check", 'true') : localStorage.removeItem("login-check")),
        tap(res => res.status === "success" ? this.router.navigate(['home']) : null)
      )
  }
}
