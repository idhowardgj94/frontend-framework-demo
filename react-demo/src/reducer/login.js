const initialState = {
  isLogin: false,
  status: 'init',
  data: []
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case 'LOGIN_REQUEST':
    return { ...state, ...payload }

  default:
    return state
  }
}
