const initialState = {
  status: 'init',
  data: []
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

  case 'FETCH_DATA_REQUEST':
    return { ...state, ...payload }

  default:
    return state
  }
}
