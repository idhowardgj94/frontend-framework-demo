import { combineReducers } from 'redux'
import login from './login'
import data from './data'

const rootReducer = combineReducers({
  login,
  data
});

export default rootReducer;