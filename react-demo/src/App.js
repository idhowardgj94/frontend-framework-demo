import React from 'react';
import './App.css';
import 'bootstrap/scss/bootstrap.scss'
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux'
import reducer from './reducer'
import {
  BrowserRouter as Router,
  Switch,
  Route, Redirect
} from "react-router-dom";
import { Transient } from './containers/Transient';
import Login from './containers/Login'


const enhancers = []

const middleware = [
  thunk
];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const store = createStore(reducer, undefined, composedEnhancers);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/" component={Transient} />
          <Redirect to="/" />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
