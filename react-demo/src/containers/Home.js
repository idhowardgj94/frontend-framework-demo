import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";
import { fetchDataAsync } from "../actions/data";

const Container = styled.div`
  text-align: center;
`;
const Warning = styled.h3`
  color: black;
  text-align: center;
`;
const Table = styled.table``;

export class Home extends Component {
  static propTypes = {
    history: PropTypes.object,
    fetchDataAsync: PropTypes.func,
    dState: PropTypes.shape({
      status: PropTypes.string,
      data: PropTypes.array,
    }),
  };

  componentDidMount() {
    console.log('inside componentDidMount');
    this.props.fetchDataAsync()
  }

  render() {
    const dState = this.props.dState;
    if (dState.status !== "success") {
      return (
        <Container>
          <Warning>讀取中，請稍候...</Warning>
        </Container>
      );
    } else {
      return (
        <Container className="container">
          <Table className="table">
            <thead>
              <tr>
                <th scope="col"> # </th>
                <th scope="col"> 姓名 </th>
                <th scope="col"> 身高 </th>
                <th scope="col"> 體重 </th>
              </tr>
            </thead>
            <tbody>
              {dState.data.map(d => (
                <tr key={`data${d.id}`}>
                  <td> {d.id} </td>
                  <td> {d.name} </td>
                  <td> {`${d.height} 公分`} </td>
                  <td> {`${d.weight} 公斤`} </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  dState: state.data,
});

const mapDispatchToProps = {
  fetchDataAsync,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
