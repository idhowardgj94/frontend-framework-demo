import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Redirect, Route
} from "react-router-dom";
import Home from './Home';



export class Transient extends Component {
  static propTypes = {
    history: PropTypes.object,
    location: PropTypes.object
  };

  isLogin = true;

  componentDidMount() {
    console.log("inside transient componentDidMount", this.props.location);
  }

  checkLogin() {
    // check login here
    const check = localStorage.getItem('login-check');
    return check !== undefined && check !== null;
  }

  render() {
    const pathname = this.props.location
    console.log(pathname)
    return (!this.checkLogin() && pathname !== '/login') ? (
      <Redirect to={{
        pathname: '/login'
      }} />
    ): (
      <Route>
        <Route path="/home" component={Home} />
      </Route>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Transient);
