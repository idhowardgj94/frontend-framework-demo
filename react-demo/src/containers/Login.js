import React, { Component } from "react";
import { connect } from "react-redux";
import { loginAsync } from '../actions/login';
import PropTypes from 'prop-types';
export class Login extends Component {
  errMsg = "";

  static propTypes = {
    loginAsync: PropTypes.func,
    history: PropTypes.func,
    login: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      form: {
        username: {
          value: "",
          isValidate: true,
        },
        password: {
          value: "",
          isValidate: true,
        },
      },
    };
  }

  componentDidMount() {
    console.log("inside componentDidMount", this.props);
  }

  componentDidUpdate() {
    console.log("inside componentDidUpdate")
  }

  onUsernameChangeHandler(e) {
    const state = this.state;
    state.form.username.value = e.target.value;
    this.setState(state);
  }

  onPasswordChangeHandler(e) {
    const state = this.state;
    state.form.password.value = e.target.value;
    this.setState(state);
  }

  onSubmitHandler(e) {
    e.preventDefault();
    const state = this.state;
    state.form.username.isValidate = state.form.username.value !== '';
    state.form.password.isValidate = state.form.password.value !== '';
    
    if(state.form.username.isValidate && state.form.password.isValidate) {
      console.log('login request here');
      const payload = {
        username: this.state.form.username.value,
        password: this.state.form.password.value
      }

      // call api here
      this.props.loginAsync(this.props.history, payload);
    } else {
      this.setState(state);
    }
  }

  render() {
    const loginState = this.props.login;
    this.errMsg = loginState.status === 'fail' ? '請檢查帳號密碼' : ''; 
    return (
      <div className="container">
        <form>
          <h3>react demo. 請登入</h3>

          <div className="form-group">
            <label>帳號</label>
            <input
              type="username"
              className={`form-control ${
                this.state.form.username.isValidate ? "" : "is-invalid"
              }`}
              placeholder="輸入帳號"
              onChange={(e) => this.onUsernameChangeHandler(e)}
            />
            <div className="invalid-feedback">請輸入合法帳號</div>
          </div>

          <div className="form-group">
            <label>密碼</label>
            <input
              onChange={(e) => this.onPasswordChangeHandler(e)}
              type="password"
              className={`form-control ${
                this.state.form.password.isValidate ? "" : "is-invalid"
              }`}
              placeholder="輸入密碼"
            />
            <div className="invalid-feedback">請輸入密碼</div>
          </div>

          {this.errorMsg !== "" && <p className="text-danger">{this.errMsg}</p>}
          <button
            type="submit"
            className="btn btn-primary btn-block"
            onClick={(e) => this.onSubmitHandler(e)}
          >
            登入
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  login: state.login
});

const mapDispatchToProps = {
  loginAsync
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
