export const fetchDataAsync = (history, payload) => dispatch => {
  dispatch(fetchDataRequest({
    status: 'init',
    data: []
  }));

  fetch('/api/v1/data', {
    method: 'GET',
  }).then(r => r.json())
    .then(d => {
        dispatch(fetchDataRequest({
          ...d
         }));
    });
}

export const fetchDataRequest = (payload) => ({
  type: 'FETCH_DATA_REQUEST',
  payload
})