export const loginAsync = (history, payload) => dispatch => {
  console.log('inside login handler');
  dispatch(loginRequest({
    status: 'init',
    data: []
  }));

  fetch('/api/v1/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }).then(r => r.json())
    .then(d => {
      if(d.status === 'success') {
        dispatch(loginRequest({
          status: 'success', 
          data: []
         }));
         localStorage.setItem('login-check', true);
         history.push('/home');
      } else {
        dispatch(loginRequest({
          status: 'fail'
        }));
      }
    });
}

export const loginRequest = (payload) => ({
  type: 'LOGIN_REQUEST',
  payload
})